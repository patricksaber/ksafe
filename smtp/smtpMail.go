package smtp

import (
	"crypto/tls"
	"fmt"
	"log"
	"net/smtp"
	"strings"
)

type Mail struct {
	senderId string
	toIds    []string
	Cc       []string
	Bcc      []string
	subject  string
	body     string
}

type SmtpServer struct {
	host      string
	port      string
	TlsConfig *tls.Config
}

func (s *SmtpServer) ServerName() string {
	return s.host + ":" + s.port
}

func (mail *Mail) BuildMessage() string {
	message := ""
	message += fmt.Sprintf("From: %s\r\n", mail.senderId)
	if len(mail.toIds) > 0 {
		message += fmt.Sprintf("To: %s\r\n", strings.Join(mail.toIds, ";"))
	}
	if len(mail.Cc) > 0 {
		message += fmt.Sprintf("Cc: %s\r\n", strings.Join(mail.Cc, ";"))
	}
	mime := "MIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\";\n\n"
	message += fmt.Sprintf("Subject: %s\r\n", mail.subject)
	message += mime + mail.body
	//message += "\r\n" + mail.body
	return message
}

func SmtpMail(email string, message string, userId string) (sent bool, err error) {
	var mesg = "<html><body>Urgent<br><br>UserID " + userId + " <br><br> <h3><b>" + message + "</b></h3>" +
		"<br><br> Yours sincerely,<br><br> KSafe Backend</body></html>"

	mail := Mail{}
	mail.senderId = "p.anonymouscoin1@gmail.com"
	//mail.toIds = []string{"dhal.asitk@gmail.com", "asitdhal_tud@outlook.com"}
	mail.toIds = []string{email}
	//mail.Cc =[]string{"marounabisaber@gmail.com","jackyabouhaidar@gmail.com"}
	mail.subject = "KSafe Urgent"
	mail.body = mesg

	messageBody := mail.BuildMessage()

	smtpServer := SmtpServer{host: "smtp.gmail.com", port: "465"}

	log.Println(smtpServer.host)
	//build an auth
	auth := smtp.PlainAuth("p.anonymouscoin1@gmail.com", mail.senderId, "K$@ffP@trick!123", smtpServer.host)

	// Gmail will reject connection if it's not secure
	// TLS config
	tlsconfig := &tls.Config{
		InsecureSkipVerify: true,
		ServerName:         smtpServer.host,
	}

	conn, err := tls.Dial("tcp", smtpServer.ServerName(), tlsconfig)
	if err != nil {
		fmt.Println(err)
		return false, err
	}

	client, err := smtp.NewClient(conn, smtpServer.host)
	if err != nil {
		return false, err
	}

	// step 1: Use Auth
	if err = client.Auth(auth); err != nil {
		fmt.Println(err)
		return false, err
	}

	// step 2: add all from and to
	if err = client.Mail(mail.senderId); err != nil {
		fmt.Println(err)
		return false, err
	}
	for _, k := range mail.toIds {
		if err = client.Rcpt(k); err != nil {
			fmt.Println(err)
			return false, err
		}
	}

	// Data
	w, err := client.Data()
	if err != nil {
		fmt.Println(err)
		return false, err
	}

	_, err = w.Write([]byte(messageBody))
	if err != nil {
		fmt.Println(err)
		return false, err
	}

	err = w.Close()
	if err != nil {
		fmt.Println(err)
		return false, err
	}

	client.Quit()

	log.Println("Mail sent successfully")
	return true, nil
}
