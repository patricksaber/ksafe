package model

type StatusCode struct {
	StatusCodeList StatusCodeList `json:"statusCode"`
}
type StatusCodeList struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

func GetStatusCode() ([]StatusCode, error) {
	//Mock data - implement gym information
	var statusCode []StatusCode
	statusCode = append(statusCode, StatusCode{StatusCodeList: StatusCodeList{Code: 0, Message: "Success"}})
	statusCode = append(statusCode, StatusCode{StatusCodeList: StatusCodeList{Code: 1, Message: "wrong email or password"}})
	statusCode = append(statusCode, StatusCode{StatusCodeList: StatusCodeList{Code: 2, Message: "Registration completed successfully"}})
	statusCode = append(statusCode, StatusCode{StatusCodeList: StatusCodeList{Code: 3, Message: "Email Already Exist"}})
	statusCode = append(statusCode, StatusCode{StatusCodeList: StatusCodeList{Code: 4, Message: "Failed to load userinfo"}})
	statusCode = append(statusCode, StatusCode{StatusCodeList: StatusCodeList{Code: 5, Message: "Bad Request - JSON cannot be parsed"}})
	statusCode = append(statusCode, StatusCode{StatusCodeList: StatusCodeList{Code: 6, Message: "Wrong card id"}})
	statusCode = append(statusCode, StatusCode{StatusCodeList: StatusCodeList{Code: 7, Message: "Your account has been recharged successfully"}})
	statusCode = append(statusCode, StatusCode{StatusCodeList: StatusCodeList{Code: 8, Message: "Bad Request"}})
	statusCode = append(statusCode, StatusCode{StatusCodeList: StatusCodeList{Code: 9, Message: "You do not have enough balance"}})
	statusCode = append(statusCode, StatusCode{StatusCodeList: StatusCodeList{Code: 10, Message: "You have to wait for offer expiration"}})
	statusCode = append(statusCode, StatusCode{StatusCodeList: StatusCodeList{Code: 11, Message: "User notified successfully"}})
	statusCode = append(statusCode, StatusCode{StatusCodeList: StatusCodeList{Code: 12, Message: "Unknown profile"}})
	statusCode = append(statusCode, StatusCode{StatusCodeList: StatusCodeList{Code: 13, Message: "Account expired"}})
	statusCode = append(statusCode, StatusCode{StatusCodeList: StatusCodeList{Code: 14, Message: "PinCode not sent"}})
	return statusCode, nil
}
