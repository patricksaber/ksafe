package model

type UserInfo struct {
	Email      string `json:"email"`
	PushId     string `json:"pushId"`
	AppVersion string `json:"appVersion"`
	OsVersion  string `json:"osVersion"`
	Name       string `json:"name"`
}

type DataList struct {
	ListData []ListData `json:"appList"`
}

type ListData struct {
	AppName     string `json:"appname"`
	AppPassword string `json:"apassword"`
}
