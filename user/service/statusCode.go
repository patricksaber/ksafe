package service

import (
	"bitbucket.org/patricksaber/ksafe/user/model"
)

type StatusCodeService interface {
	GetStatusCode() ([]model.StatusCode, error)
	NewStatusCode(code int, message string) model.StatusCode
}

type statusCodeService struct {
}

func (s statusCodeService) GetStatusCode() ([]model.StatusCode, error) {
	return model.GetStatusCode()
}

func (s statusCodeService) NewStatusCode(code int, message string) model.StatusCode {
	return model.StatusCode{
		StatusCodeList: model.StatusCodeList{
			Code:    code,
			Message: message,
		},
	}
}

func NewStatusCodeService() StatusCodeService {
	//later on if MySqlDb dependency not present, throw error or panic
	return statusCodeService{}
}
