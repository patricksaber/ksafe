package utils

import (
	"bytes"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"math/rand"
	"net/http"
	"os"
	"reflect"
	"strings"
	"time"
	"unsafe"

	"bitbucket.org/patricksaber/ksafe/smtp"
)

func GetPinCode() string {
	rand.Seed(time.Now().UnixNano())
	chars := []rune("ABCDEFQRSTUVWXYZ" +
		"abcdefghijklmnopq" +
		"!@#$%^&*()" +
		"0123456789")
	length := 10
	var b strings.Builder
	for i := 0; i < length; i++ {
		b.WriteRune(chars[rand.Intn(len(chars))])
	}
	str := b.String()
	return str
}

func RecoveryCSV(file string) error {

	f, err := os.Open(file)
	if err != nil {
		return err
	}
	defer f.Close()
	lines, err := csv.NewReader(f).ReadAll()
	if err != nil {
		return err
	}

	for _, line := range lines {
		strings.Join(line, " ")

		//fmt.Println(Decode64(Before(justString, " ")) + " " + Decode64(After(justString, " ")))
	}
	return nil
}

func WriteLogs(path string, message string, userId string) {
	//d1 := []byte(message+"\n")
	//err := ioutil.WriteFile("logs/"+path, d1, 0644)
	//check(err)
	f, err := os.OpenFile("logs/"+path,
		os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Println(err)
	}
	defer f.Close()

	logger := log.New(f, "User id: "+userId+"\n", log.LstdFlags)
	logger.Println(message + "\n")
}

func Between(value string, a string, b string) string {
	// Get substring between two strings.
	posFirst := strings.Index(value, a)
	if posFirst == -1 {
		return ""
	}
	posLast := strings.Index(value, b)
	if posLast == -1 {
		return ""
	}
	posFirstAdjusted := posFirst + len(a)
	if posFirstAdjusted >= posLast {
		return ""
	}
	return value[posFirstAdjusted:posLast]
}

func Before(value string, a string) string {
	// Get substring before a string.
	pos := strings.Index(value, a)
	if pos == -1 {
		return ""
	}
	return value[0:pos]
}

func After(value string, a string) string {
	// Get substring after a string.
	pos := strings.LastIndex(value, a)
	if pos == -1 {
		return ""
	}
	adjustedPos := pos + len(a)
	if adjustedPos >= len(value) {
		return ""
	}
	return value[adjustedPos:len(value)]
}

func CreateFoldersIfNotExsist() {
	if _, err := os.Stat("logs/"); os.IsNotExist(err) {
		os.Mkdir("logs/", os.ModePerm)
		fmt.Println("mkdr1")
	}

	if _, err := os.Stat("sharedFolder/backupCSV/"); os.IsNotExist(err) {
		os.MkdirAll("sharedFolder/backupCSV/", os.ModePerm)
		fmt.Println("mkdr2")
	}

}

func SentUrgentSmpt(message string, userId string) error {
	sent, err := smtp.SmtpMail("patrickabisaber@gmail.com", message, userId)
	if err != nil {
		return err
	}
	if sent {
		return nil
	}
	return nil
}

func IoReaderToStringDecyptToIoReader(reader io.Reader) (result io.Reader) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(reader)
	s := Decrypt(buf.String())
	return strings.NewReader(s)
}
func RespondwithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	result := BytesToString(response)
	w.Write([]byte(Encrypt(result)))
}
func BytesToString(b []byte) string {
	bh := (*reflect.SliceHeader)(unsafe.Pointer(&b))
	sh := reflect.StringHeader{bh.Data, bh.Len}
	return *(*string)(unsafe.Pointer(&sh))
}
