package calls

import (
	"encoding/json"
	"fmt"
	"net/http"

	"bitbucket.org/patricksaber/ksafe/config"
	"bitbucket.org/patricksaber/ksafe/user/databaseModel"
	"bitbucket.org/patricksaber/ksafe/user/model"
	"bitbucket.org/patricksaber/ksafe/user/service"
	"bitbucket.org/patricksaber/ksafe/user/utils"
)

var statusCodes, _ = statusCodeService.GetStatusCode()
var statusCodeService = service.NewStatusCodeService()

func LoginUser(w http.ResponseWriter, router *http.Request) {

	w.Header().Set("Content-Type", "application/json")

	appLang := utils.Decrypt(router.Header.Get("idAppLanguage"))
	appVersion := utils.Decrypt(router.Header.Get("appVersion"))
	osVersion := utils.Decrypt(router.Header.Get("osVersion"))

	var t model.UserInfo

	result := utils.IoReaderToStringDecyptToIoReader(router.Body)
	err1 := json.NewDecoder(result).Decode(&t)

	if err1 != nil {
		utils.WriteLogs("login", err1.Error(), t.Email)
		utils.RespondwithJSON(w, http.StatusBadRequest, statusCodes[5])
		return
	}
	created := loginDb(t, osVersion, appVersion, appLang)
	if created {
		utils.RespondwithJSON(w, http.StatusOK, statusCodes[0])
	}

}
func loginDb(info model.UserInfo, osV string, appV string, lang string) (message bool) {
	db, err := config.GetMySQLDB()
	if err != nil {
		fmt.Println(err.Error())
		return false
	} else {
		productModel := databaseModel.ProductModel{
			Db: db,
		}
		_, err := productModel.Registration(info, osV, appV, lang)
		if err != nil {
			fmt.Println(err.Error())
			return false

		}
		return true
	}
	return
}

//func loginDb(email string, password string) (exist bool) {
//	db, err := config.GetMySQLDB()
//	if err != nil {
//		fmt.Println(err)
//	} else {
//		productModel := databaseModel.ProductModel{
//			Db: db,
//		}
//		ex, err := productModel.CheckDbLogin(email, password)
//		if err != nil {
//			fmt.Println(err)
//
//		}
//		if ex {
//			return true
//		}
//		return false
//	}
//	return
//}
