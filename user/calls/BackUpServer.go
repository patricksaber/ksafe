package calls

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"bitbucket.org/patricksaber/ksafe/config"
	"bitbucket.org/patricksaber/ksafe/user/databaseModel"
	"bitbucket.org/patricksaber/ksafe/user/utils"
)

func checkError(message string, err error) {
	if err != nil {
		log.Fatal(message, err)
	}
}

//func BackUp(w http.ResponseWriter, router *http.Request) {
//	w.Header().Set("Content-Type", "application/json")
//	email := w.Header().Get("emailAddress")
//
//	var body model.DataList
//	err := json.NewDecoder(router.Body).Decode(&body)
//	if err != nil {
//		json.NewEncoder(w).Encode(statusCodes[5])
//		return
//	}
//	file, err := os.Create(email + ".csv")
//	wr := csv.NewWriter(file)
//	checkError("Cannot create file", err)
//	defer file.Close()
//	for i := 0; i < len(body.ListData); i++ {
//		x := []string{utils.Encode64(body.ListData[i].AppName), utils.Encode64(body.ListData[i].AppPassword)}
//		xx := [][]string{x}
//		wr.WriteAll(xx)
//		wr.Flush()
//	}
//	response := model.ResponseBackUp{
//		ResponseStatus: model.ResponseStatus{
//			Code:    0,
//			Message: "success",
//		},
//	}
//	json.NewEncoder(w).Encode(response)
//	utils.RecoveryCSV("result.csv")
//}

const MaxMemory = 0.5 * 1024 * 1024

func ReadCSVFromHttpRequest(w http.ResponseWriter, router *http.Request) {
	utils.CreateFoldersIfNotExsist()
	var email = router.Header.Get("emailAddress")
	router.Body = http.MaxBytesReader(w, router.Body, MaxMemory)
	err := router.ParseMultipartForm(MaxMemory)
	if err != nil {
		newStatusCode := statusCodeService.NewStatusCode(24, "the size must be maximum 0.5 Mb")
		json.NewEncoder(w).Encode(&newStatusCode)
		utils.WriteLogs("backupCSV", err.Error(), email)
		return
	}

	file, _, err := router.FormFile("backupCSV")
	if err != nil {
		utils.WriteLogs("profile", "Error retrieving file from form-data", "114")
		return
	}
	defer file.Close()
	utils.Before(email, "@")

	fName, er := saveCsvDB(email)
	if er != nil {
		newStatusCode := statusCodeService.NewStatusCode(24, "you already backup the file")
		json.NewEncoder(w).Encode(&newStatusCode)
		return
	}

	f, err := os.Create("sharedFolder/backupCSV/" + fName)

	if err != nil {
		utils.WriteLogs("urgent", err.Error(), email)
		err := utils.SentUrgentSmpt(err.Error(), email)
		if err != nil {
			utils.WriteLogs("smtp", "not created", "backend")
		}
		err1 := DeleCsvDB(fName)
		if err1 != nil {
			utils.WriteLogs("urgent", err1.Error(), "backend")
		}

		newStatusCode := statusCodeService.NewStatusCode(404, "server error, try again later")
		json.NewEncoder(w).Encode(&newStatusCode)
		return
	}
	defer f.Close()

	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		utils.WriteLogs("backupCSV", err.Error(), email)
		utils.WriteLogs("urgent", err.Error(), email)
		newStatusCode := statusCodeService.NewStatusCode(404, "server error")
		json.NewEncoder(w).Encode(&newStatusCode)
		return
	}
	f.Write(fileBytes)
	utils.Copy("sharedFolder/backupCSV/", "backupCSV/")
	json.NewEncoder(w).Encode(statusCodes[0])
	//fmt.Fprintf(w, "Sussessfully Uploaded File\n")
}

func saveCsvDB(email string) (file string, err error) {
	db, err := config.GetMySQLDB()
	if err != nil {
		fmt.Println(err.Error())
		return "", err
	} else {
		productModel := databaseModel.ProductModel{
			Db: db,
		}
		fileN, err := productModel.SaveCSV(email)
		if err != nil {
			return "", err
		}
		return fileN, nil
	}
	return
}
func DeleCsvDB(fileName string) error {
	db, err := config.GetMySQLDB()
	if err != nil {
		fmt.Println(err.Error())
		return err
	} else {
		productModel := databaseModel.ProductModel{
			Db: db,
		}
		err := productModel.DeleteCSV(fileName)
		if err != nil {
			return err
		}
		return nil
	}
	return nil
}
