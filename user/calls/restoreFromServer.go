package calls

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/patricksaber/ksafe/config"
	"bitbucket.org/patricksaber/ksafe/user/databaseModel"
	"bitbucket.org/patricksaber/ksafe/user/model"
	"bitbucket.org/patricksaber/ksafe/user/utils"
)

func RestoreCSV(w http.ResponseWriter, router *http.Request) {
	var email = router.Header.Get("emailAddress")
	item, err := getCsvDB(email)
	if err != nil {
		utils.WriteLogs("urgent", err.Error(), email)
		newStatusCode := statusCodeService.NewStatusCode(404, "server error")
		json.NewEncoder(w).Encode(&newStatusCode)
		err := utils.SentUrgentSmpt(err.Error(), email)
		if err != nil {
			utils.WriteLogs("smtp", "not created", "backend")
		}
		return
	} else if item != nil {
		response := model.ResponseBackUp{
			ResponseStatus: model.ResponseStatus{
				Code:    0,
				Message: "Success",
			},
			Data: item,
		}

		json.NewEncoder(w).Encode(response)
		return
	}
	newStatusCode := statusCodeService.NewStatusCode(24, "no backup")
	json.NewEncoder(w).Encode(&newStatusCode)
}

func getCsvDB(email string) (file []string, err error) {
	db, err := config.GetMySQLDB()
	if err != nil {
		return nil, err
	} else {
		productModel := databaseModel.ProductModel{
			Db: db,
		}
		fileN, err := productModel.GetCSV(email)
		if err != nil {
			return nil, err
		}
		return fileN, nil
	}
	return
}
