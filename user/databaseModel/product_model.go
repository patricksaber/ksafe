package databaseModel

import (
	"database/sql"
	"fmt"
	"time"

	"bitbucket.org/patricksaber/ksafe/user/model"
	"bitbucket.org/patricksaber/ksafe/user/utils"
)

type ProductModel struct {
	Db *sql.DB
}

/*func (productModel ProductModel) FindAll() ([]entities.Product, error) {
	rows, err := productModel.Db.Query("select * from demo")
	if err != nil {
		return nil, err
	} else {
		products := []entities.Product{}
		for rows.Next() {
			var id int64
			var name string
			var price float32
			var quantity int
			err2 := rows.Scan(&id, &name, &price, &quantity)
			if err2 != nil {
				return nil, err2
			} else {
				product := entities.Product{id, name, price, quantity}
				products = append(products, product)
			}
		}
		return products, nil
	}
}*/

func (productModel ProductModel) InsertData() error {
	insForm, err := productModel.Db.Query("INSERT INTO `userInfo` (`id`, `name`, `price`, `quantity`) VALUES (NULL, 'Zaatar', '1.5', '50')")
	if err != nil {
		return err
	}
	fmt.Println(insForm)
	return nil
}

func (productModel ProductModel) DeleteData() error {
	insForm, err := productModel.Db.Query("DELETE FROM userInfo WHERE name='zaatar'")
	if err != nil {
		return err
	}
	fmt.Println(insForm)
	return nil
}

func (productModel ProductModel) CheckDbLogin(emailAd string, password string) (exist bool, err error) {
	var inf = false
	insForm, err := productModel.Db.Query("SELECT userInfo.Email FROM userInfo WHERE Email='" + emailAd + "' AND password='" + password + "'")
	if err != nil {
		return false, err
	}
	info := model.UserInfo{}
	for insForm.Next() {
		var email string
		err = insForm.Scan(&email)
		if err != nil {
			panic(err.Error())
		}
		info.Email = email
		if emailAd == email {
			return true, nil
		}
	}

	return inf, nil
}

var currentTime = time.Now()

func (productModel ProductModel) Registration(info model.UserInfo, osVersion string, appVersion string, appLang string) (message string, err error) {
	//check if userexist first
	we, err := productModel.Db.Query("SELECT 1 FROM userInfo WHERE Email='" + info.Email + "'")
	if err != nil {
		fmt.Println(err.Error())
		return "Error Connecting to database", err
	}
	for we.Next() {
		var i int
		err = we.Scan(&i)
		if err != nil {
			panic(err.Error())
		}
		_, err := productModel.Db.Query("UPDATE `userInfo` SET `Email` = '" + info.Email + "',`AppLang` = '" + appLang + "', `PushId` = '" + info.PushId + "', " +
			"`Name` = '" + info.Name + "', `OsVersion` = '" + osVersion + "', `AppVersion` = '" + appVersion + "' WHERE `Email` ='" + info.Email + "' ")
		if err != nil {
			fmt.Println(err.Error())
			return "Error Connecting to database", err
		}
		return "updated", nil
	}
	// while not exist create account
	var time = currentTime.AddDate(0, 0, 0).Format("2006-01-02")

	_, err2 := productModel.Db.Query("INSERT INTO `userInfo` (`userId`, `Email`, `PushId`, `Name`, `OsVersion`, `AppVersion`, `AppLang`,`AccountCreate`, `Active`)" +
		" VALUES (NULL, '" + info.Email + "', '" + info.PushId + "', '" + info.Name + "', '" + osVersion + "', '" + appVersion + "','" + appLang + "', '" + time + "', '0')")
	if err2 != nil {
		fmt.Println(err2.Error())
		return "Error Connecting to database", err
	}
	return "created", nil

}

//func (productModel ProductModel) Registration(info model.UserInfo) (message string, err error) {
//	//check if userexist first
//	we, err := productModel.Db.Query("SELECT 1 FROM userInfo WHERE Email='" + info.Email + "'")
//	if err != nil {
//		fmt.Println(err.Error())
//		return "Error Connecting to database", err
//	}
//	for we.Next() {
//		var i int
//		err = we.Scan(&i)
//		if err != nil {
//			panic(err.Error())
//		}
//		return "exist", nil
//	}
//	// while not exist create account
//	var time = currentTime.AddDate(0, 0, 0).Format("2006-01-02")
//	_, err2 := productModel.Db.Query("INSERT INTO `userInfo`(`userId`,`Email`,`password`,`FirstName`,`LastName`,`Active`," +
//		"`AccountCreate`) VALUES (NULL, '" + info.Email + "','" + info.Password + "','" + info.Firstname + "','" + info.Lastname + "','" + "0" + "','" + time + "')")
//	if err2 != nil {
//		fmt.Println(err2.Error())
//		return "Error Connecting to database", err
//	}
//	var pincode = utils.GetPinCode()
//	_, err3 := productModel.Db.Query("INSERT INTO `pincode`(`Email`,`PinCode`) " +
//		"VALUES ('" + info.Email + "','" + pincode + "')")
//	if err3 != nil {
//		return "Error Connecting to database", err
//	}
//
//	return "created email not sent", nil
//
//}

func (productModel ProductModel) SaveCSV(email string) (file string, er error) {
	var time = currentTime.AddDate(0, 0, 0).Format("2006-01-02")
	var fileName = utils.Before(email, "@") + time + ".csv"
	_, err := productModel.Db.Query("INSERT INTO `backUp` (`Email`, `csv`,`time`) VALUES ('" + email + "', '" + fileName + "', '" + time + "')")
	if err != nil {
		return "", err
	}
	return fileName, nil
}
func (productModel ProductModel) DeleteCSV(filename string) error {
	_, err := productModel.Db.Query("DELETE FROM backUp WHERE csv = '" + filename + "'")

	if err != nil {
		return err
	}
	return nil
}
func (productModel ProductModel) GetCSV(email string) (file []string, err error) {
	var files []string
	rows, err := productModel.Db.Query("SELECT backUp.csv FROM `backUp` WHERE Email ='" + email + "'")
	if err != nil {
		return nil, err
	} else {
		for rows.Next() {
			var name string
			err2 := rows.Scan(&name)
			if err2 != nil {
				return nil, err2
			} else {
				product := name
				files = append(files, product)
			}
		}
		return files, nil
	}
}
