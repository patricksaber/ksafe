package main

import (
	"fmt"
	"log"
	"net/http"

	"bitbucket.org/patricksaber/ksafe/config"
	"bitbucket.org/patricksaber/ksafe/user/calls"
	"bitbucket.org/patricksaber/ksafe/user/databaseModel"
	"github.com/gorilla/mux"

	"bitbucket.org/patricksaber/ksafe/user/utils"
)

const (
	STATIC_DIR = "/sharedFolder"
	serverKey  = "AIzaSyAUIGmQHqS5NJXRxyqbQsOIOsxvMm4sZD8"
	topic      = "/topics/someTopic"
)

func checkError(message string, err error) {
	if err != nil {
		log.Fatal(message, err)
	}
}

var data = [][]string{{"Line1", "Hello Readers of"}, {"Line2", "golangcode.com"}}

// var listtt []DataList
func main() {
	utils.CreateFoldersIfNotExsist()

	router := mux.NewRouter().StrictSlash(true)
	router.PathPrefix(STATIC_DIR).Handler(http.StripPrefix(STATIC_DIR, http.FileServer(http.Dir("."+STATIC_DIR))))
	router.HandleFunc("/api/user/Login", calls.LoginUser).Methods("POST")
	//router.HandleFunc("/api/user/Registration", calls.Registration).Methods("POST")
	router.HandleFunc("/api/user/BackUp", calls.ReadCSVFromHttpRequest).Methods("POST")
	router.HandleFunc("/api/user/Restore", calls.RestoreCSV).Methods("GET")
	log.Fatal(http.ListenAndServe(":8880", router))

}

/*func demo() {
	db, err := config.GetMySQLDB()
	if err != nil {
		fmt.Println(err)
	} else {
		productModel := databaseModel.ProductModel{
			Db: db,
		}
		products, err := productModel.FindAll()
		if err != nil {
			fmt.Println(err)
		} else {
			fmt.Println(products)
			fmt.Println("List")
			for _, products := range products {
				fmt.Println("id", products.Id)
				fmt.Println("name", products.Name)
				fmt.Println("price", products.Price)
				fmt.Println("quantity", products.Quantity)

			}
		}
	}
}*/

func insert() {
	db, err := config.GetMySQLDB()
	if err != nil {
		fmt.Println(err)
	} else {
		productModel := databaseModel.ProductModel{
			Db: db,
		}
		err := productModel.InsertData()
		if err != nil {
			fmt.Println(err)
		}
	}

}
func delete() {
	db, err := config.GetMySQLDB()
	if err != nil {
		fmt.Println(err)
	} else {
		productModel := databaseModel.ProductModel{
			Db: db,
		}
		err := productModel.DeleteData()
		if err != nil {
			fmt.Println(err)
		}
	}
}
